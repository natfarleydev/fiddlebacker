import { isCollection } from 'immutable'
import * as Note from 'tonal-note'
import * as Key from 'tonal-key'
import * as assert from 'assert'

/**
 * Take Tonal.js note and convert it to ABC.js note.
 *
 * It can also take a collection of notes and turn them into a single chord in
 * abc notation
 *
 * Made with heavy inspiration from tonal-abc-notation.
 * @param {String} tonalNote Note from Tonal.js
 * @param {String} key The key the piece is in
 * @returns {String} ABC representation of the note/chord.
 */
export function toAbc (tonalNote, key) {
  if (isCollection(tonalNote)) {
    return '[' + tonalNote.reduce((acc, val) => acc + toAbc(val, key), '') + ']'
  } else {
    const { letter, acc, oct } = Note.props(tonalNote)
    const scale = Key.scale(key)
    // The accidental for ABC
    let a
    if (scale.includes(letter + acc)) {
      a = ''
    } else {
      a = acc === '' ? '=' : acc.replace(/b/g, '_').replace(/#/g, '^')
    }

    // Setting the octave
    let o
    if (oct === 5) {
      o = ''
    } else if (oct > 5) {
      o = Array(oct - 5).join("'")
    } else if (oct < 5) {
      o = Array(6 - oct).join(',')
    }

    return a + letter + o
  }
}

/**
 *
 * @param {String} abcDuration String representing the abc duration
 */
export function abcDurationToNumber (abcDuration) {
  if (abcDuration === '') {
    return 1
  } else if (abcDuration.split('').every(x => x === '/')) {
    return 1 / 2 ** (abcDuration.length)
  } else if (Number(abcDuration)) {
    return Number(abcDuration)
  } else if (abcDuration.endsWith('<')) {
    return abcDurationToNumber(abcDuration.slice(0, -1)) * 0.5
  } else if (abcDuration.endsWith('>')) {
    return abcDurationToNumber(abcDuration.slice(0, -1)) * 1.5
  } else {
    assert(false, "Shouldn't get here...")
  }
}

export function durationOfList (list) {
  return list.reduce((totalDuration, note, ind, array) => {
    const duration = abcDurationToNumber(note.get('duration'))

    const prevNote = array.get(ind - 1)
    if (prevNote) {
      // Test if this note is the end of a dotted pair
      if (prevNote.get('duration').endsWith('<')) {
        return totalDuration + duration * 1.5
      } else if (prevNote.get('duration').endsWith('>')) {
        return totalDuration + duration * 0.5
      }
    }

    // If there is not a previous note OR there is a previous note but it is not
    // the start of a dotted pair
    return totalDuration + duration
  }, 0)
}
