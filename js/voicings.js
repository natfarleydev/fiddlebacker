import { Chord, Distance, Note } from 'tonal'
import * as assert from 'assert'
import { Seq, fromJS } from 'immutable'

/**
 *
 * @param {String} note Tonal.js note
 */
export function inChordalRange (note) {
  return (Distance.semitones(note, 'F4') < 0 &&
        Distance.semitones(note, 'Ab5') > 0)
}

/**
 *
 * @param {Seq} notesSeq
 */
// export function allInChordalRange (notesSeq) {
//   return notesSeq.reduce((x, y) => x && y.every(inChordalRange), true)
// }

export function sortNotesAscending (note, otherNote) {
  return Note.freq(note) > Note.freq(otherNote)
}

export function sortNotesDescending (note, otherNote) {
  return Note.freq(note) < Note.freq(otherNote)
}

/**
 *
 * @param {String[]} chord A Tonal.js chord as an array.
 */
export function invertDown (chord) {
  return Seq(chord)
    .sort(sortNotesAscending)
    .update(seq => {
      return Seq([...seq.butLast(), Distance.transpose(seq.last(), '-8P')])
    })
    .sort(sortNotesAscending)
}

/**
 *
 * @param {Seq} chord1
 * @param {Seq} chord2
 */
export function calcLeading (chord1, chord2) {
  assert.strictEqual(chord1.size, chord2.size,
    'Chords must be the same size to calculate leading. Got ' +
        chord1.size + ' and ' + chord2.size)
  return chord1
    .map((val, ind) => Distance.semitones(val, chord2.get(ind)))
    .reduce((x, y) => x + Math.abs(y), 0)
}

/**
 *
 * @param {Seq} chord
 * @param {Number} prevDarkness
 */
export function calcDarkness (chord, prevDarkness) {
  const noOfDarkNotes = chord
    .map(x => Distance.semitones('C5', x) < 0 ? 1 : 0)
    .reduce((x, y) => x + y)
    // If this chord is 'dark' (i.e. with two or more notes below C5), add darkness
  if (noOfDarkNotes > 1) {
    return prevDarkness + 1
  } else {
    return 0
  }
}

/**
 * Gets a voicing of the notes for the right hand
 * @param {String} symbol Chord symbol to get the notes of
 */
export function getVoicing (symbol, lastChord) {
  const _lastChord = lastChord || fromJS(['C5', 'C5', 'C5'])
  // let notes = Seq(Chord.notes(chord)).map(x => x + '5').sort(sortNotesAscending)

  const voicing = Seq(Chord.notes(symbol))
    .map(x => x + '5')
    .slice(-3)
    .sort(sortNotesAscending)

  const x = Seq([
    voicing.sort(sortNotesAscending),
    invertDown(voicing),
    invertDown(invertDown(voicing)),
    invertDown(invertDown(invertDown(voicing)))
  ])
  // Filter out chords that are out of range
    .filter(x => x.every(inChordalRange))
    // Filter out chords that are too dark
    .filter(x => calcDarkness(x, 0) === 0)
    // Sort by leading
    .sort(v => calcLeading(v, _lastChord))
    // Get the chord with the least leading
    .first()

  return x
}
