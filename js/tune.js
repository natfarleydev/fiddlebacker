import _ from 'lodash'
import * as assert from 'assert'
import { Chord, Distance, Scale } from 'tonal'
import { Seq, Map, Repeat, List, fromJS, isCollection } from 'immutable'

import {
  divChordsToChordBlocksRdcr
} from './reducers'

import { toAbc, durationOfList } from './abcconversions'
import { getVoicing } from './voicings'

/**
 * Returns sensible bass notes.
 * @param {String} chord
 * @returns {Seq}
 */
export function getBassNotes (chord) {
  let notes = Seq(Chord.notes(chord)).map((x) => x + '3')
  return notes.slice(0, 3)
}

/**
 * Returns an object as a ABC header string (for piano)
 * @param {object} h All the info that should be in the header
 * @param {String} h.T Title
 * @param {String} h.C Composer
 * @param {String} h.M Time signature
 * @param {String} h.Q Tempo
 * @param {String} h.K Key (e.g. 'D major' or 'E minor', *not* simply 'D')
 * @param {String} h.L Unit of time for abc that not durations are based on e.g. 1/8 for quavers
 * @param {String} h.Z Note
 */
function createABCHeader (h) {
  let retString = ''
  for (let i in h) {
    retString += i + ': ' + h[i] + '\n'
  }
  retString += '%%staves (RH) (LH)\n'
  retString += 'V:RH clef=treble name =""\n'
  retString += 'V:LH clef=bass name =""\n'

  return retString
}

export function getRhythmBlock (timeSig) {
  const rhythmBlocks = Map({
    '4/4': fromJS({
      rh: [
        { rest: true, duration: '' },
        { rest: false, duration: '/<' },
        { rest: true, duration: '/' }
      ],
      lh: [
        { rest: false, duration: '' },
        { rest: true, duration: '' }
      ],
      duration: 2
    }),
    '2/4': fromJS({
      rh: [
        { rest: true, duration: '/' },
        { rest: false, duration: '//<' },
        { rest: true, duration: '//' }
      ],
      lh: [
        { rest: false, duration: '/' },
        { rest: true, duration: '/' }
      ],
      duration: 1
    }),
    '6/8': fromJS({
      rh: [
        { rest: true, duration: '2' },
        { rest: false, duration: '' }
      ],
      lh: [
        { rest: false, duration: '2' },
        { rest: true, duration: '' }
      ],
      duration: 3
    }),
    '3/4': fromJS({
      rh: [
        { rest: true, duration: '' },
        { rest: false, duration: '/' },
        { rest: true, duration: '/' },
        { rest: false, duration: '/' },
        { rest: true, duration: '/' }
      ],
      lh: [
        { rest: false, duration: '' },
        { rest: true, duration: '2' }
      ],
      duration: 3
    })
  })

  // Return a collection of rhythm blocks which have the chord symbol in it
  return rhythmBlocks.get(timeSig)
}

/**
 * Returns a List of rhythmBlocks with the chordBlock added.
 * @param {String} timeSig The time signature (for finding the right rhythm)
 * @param {List} rhythmBlocks previously calculated rhythmBlocks
 * @param {Map} chordBlock a Map with `symbol` and `duration` defined
 */
export function chordBlockToRhythmBlocks (timeSig, rhythmBlocks, chordBlock) {
  const singleRhythmBlock = getRhythmBlock(timeSig)

  assert(chordBlock.get('duration') % singleRhythmBlock.get('duration') === 0,
    'The rhythm block is incompatible with the ' +
    "chord block. I'm not sure what to do." +
    `rhythm block duration: ${singleRhythmBlock.get('duration')}` +
    ` and chord block duration: ${chordBlock.get('duration')}`)

  const currentRhythmBlocks = Repeat(
    singleRhythmBlock,
    chordBlock.get('duration') / singleRhythmBlock.get('duration')
  )

  return List([
    ...rhythmBlocks,
    ...currentRhythmBlocks.map(x => x.set('symbol', chordBlock.get('symbol')))
  ])
}

/**
 * Gets a voicing of the notes for the right hand
 *
 * TODO make it know the previous chord symbol to know if the current one is a
 * change so that it can know to establish the root
 *
 * TODO Minimise jumping
 *
 * TODO Allow a range greater than an octave
 * @param {String} symbol Chord symbol to get the notes of
 * @param {String} prevNote TODO make this notes so this function can be clever
 * @param {String} nextChordSymbol
 */
export function newGetBassNote (symbol, prevNotes, nextChordSymbol, prevChordSymbol) {
  // TODO test this function properly
  const prevNote = isCollection(prevNotes)
    ? prevNotes
      .filterNot(x => x.get('rest'))
      .last()
      .get('notes')
      .last()
    : undefined

  const notes = Seq(Chord.notes(symbol)).map(x => x + '3')

  const theLastChordIsTheSame = symbol === prevChordSymbol
  const theLastNoteIsTheRoot = notes.first() === prevNote
  const theNextChordIsAFourthUp = Distance.transpose(symbol, '4P') === nextChordSymbol

  if (!theLastChordIsTheSame) {
    return notes.get(0)
  } else if (theNextChordIsAFourthUp) {
    return notes.get(1)
  } else if (theLastNoteIsTheRoot && theLastChordIsTheSame) {
    return notes.get(2)
  }

  // If in doubt, return the root
  return notes.get(0)
}

/**
 * Takes a rhythm block and turns it into notes
 * @param {List} notes List of notes
 * @param {Map<List, List>} rhythmBlock Map with rhythms
 * @param {Number} index Index of the current `rhythmBlock`
 * @param {List} rhythmBlocks All the rhythmBlocks
 * @returns {Map}
 */
export function rhythmBlockToNotes (notes, rhythmBlock, index, rhythmBlocks) {
  // Get the last chord's notes OR undefined if there isn't one
  const lastRHChord = notes
    .get('rh', Seq([]))
    .filterNot(x => x.get('rest'))
    .last()
    ? notes
      .get('rh')
      .filterNot(x => x.get('rest'))
      .last()
      .get('notes')
    : undefined

  // TODO make this the last *notes* rather than note
  const lastLHNotes = notes
    .get('lh', Seq([]))
    .filterNot(x => x.get('rest'))
    .last()
    ? notes
      .get('lh', Seq([]))
    : undefined

  const prevChordSymbol = rhythmBlocks.get(index - 1, List([])).get('symbol')
  const nextChordSymbol = rhythmBlocks.get(index + 1, List([])).get('symbol')
  // So, a rhythm block looks like:
  // {
  //   rh: [ {rest: true, duration: ''}],
  //   lh: [ {rest: true, duration: ''}],
  //   symbol: 'D',
  //   duration: 2
  // }

  // And we want to end up with something like:
  // {
  //   rh: [ {notes: ['D4', 'F#4', 'A4'], duration: '', symbol: 'D'}, ...]
  //   lh: [ {notes: ['D2'], duration: '', symbol:'D'}, ...]
  // }

  // Let's keep the left and right hand notes simple for now

  const rightHandNotes = rhythmBlock.get('rh')
    .map(valRH =>
      // Map: if the note is a rest, notate it as such; else, get voicing.
      valRH.get('rest')
        ? valRH.set('notes', List(['z']))
        : valRH.set('notes', getVoicing(rhythmBlock.get('symbol'), lastRHChord))
    )
  assert(rightHandNotes.every(x => x.get('notes')), 'no notes in the right hand notes? ' + rightHandNotes.get('notes'))

  const leftHandNotes = rhythmBlock.get('lh')
    .map(valLH =>
      valLH.get('rest')
        ? valLH.set('notes', List(['z']))
        : valLH.set('notes', List([newGetBassNote(rhythmBlock.get('symbol'), lastLHNotes, nextChordSymbol, prevChordSymbol)]))
    )
  assert(leftHandNotes.every(x => x.get('notes')), 'no notes in the left hand notes? ' + rightHandNotes.get('notes'))

  return notes
    .set('rh', List([...notes.get('rh', []), ...rightHandNotes]))
    .set('lh', List([...notes.get('lh', []), ...leftHandNotes]))
}

/**
 *
 * @param {Number} durationOfBar Duration of the bar in abc 'ticks'
 * @param {List} bars Bars of notes
 * @param {Map} note
 */
export function reduceIntoBars (durationOfBar, bars, note) {
  const lastBar = bars.last()

  // has the last bar been filled yet?
  if (lastBar && durationOfList(lastBar) < durationOfBar) {
    // console.log('Bar is not full With: ' + bars.last().map(v => v.get('duration')))
    return List([...bars.butLast(), lastBar.withMutations(list => list.push(note))])
  } else if (lastBar && durationOfList(lastBar) === durationOfBar) {
    // console.log('Bar is full! With: ' + bars.last().map(v => v.get('duration')))
    // console.log(durationOfList(lastBar))
    return List([...bars, List([note])])
  } else {
    return List([List([note])])
  }
}

// TODO test this function
export function listToAbcString (key, val) {
  const string = val.map(note => {
    if (note.get('rest')) {
      return 'z' + note.get('duration')
    } else {
      assert(note.get('notes'), 'This note has no notes!')
      return toAbc(note.get('notes'), key) + note.get('duration')
    }
  }).join('')

  return string
}

const timeSigProperties = Map({
  '4/4': Map({
    L: '1/4',
    duration: 4
  }),
  '2/4': Map({
    L: '1/4',
    duration: 2
  }),
  '3/4': Map({
    L: '1/4',
    duration: 3
  }),
  '6/8': Map({
    L: '1/8',
    duration: 6
  })
})

/**
 *
 * @param {List} chords The chords from the HTML
 * @param {Map} userConfig Config containing things like key, etc.
 */
export function chordsToAbcString (chords, userConfig) {
  // OK, so the config has everything we need. But it may not. Let's do some defaults.
  const defaultConfig = Map({
    timeSig: '4/4',
    key: 'D major'
  })

  assert(userConfig, 'No user config passed to chordsToAbcString!')
  const config = userConfig.withMutations(map =>
    defaultConfig.forEach((val, key) =>
      map.set(key, map.get(key) || defaultConfig.get(key))
    )
  )

  const durationOfBar = timeSigProperties.get(config.get('timeSig')).get('duration')

  const barsInALine = 4
  // Now we have the config, let's do some processing!

  // The chords come to us in a map that needs unfolding
  const chordsAllLaidOut = List(config.get('sequence').split(''))
    .map(v => chords.get(v))
    .flatten(1)

  const x = chordsAllLaidOut
    // First, we 'flatten' the chords with reduce
    .reduce(_.partial(divChordsToChordBlocksRdcr, durationOfBar), List([]))
    // Now the rhythm blocks. At the moment all rhythm blocks are of length 2,
    // but this might change in the future.
    .reduce(_.partial(chordBlockToRhythmBlocks, config.get('timeSig')), List([]))
    // Now to create notes from rhythm blocks
    .reduce(rhythmBlockToNotes, Map({ lh: List([]), rh: List([]) }))
    // Now that we've created maps of all the notes for the right and left hand,
    // it's time to reduce into bars
    .update(map => Map({
      lh: map.get('lh').reduce(_.partial(reduceIntoBars, durationOfBar), List([])),
      rh: map.get('rh').reduce(_.partial(reduceIntoBars, durationOfBar), List([]))
    }))
    // After it's reduced into bars, it needs to be mapped to have an abc string
    // per bar
    .update(map => Map({
      lh: map.get('lh').map(_.partial(listToAbcString, config.get('key'))),
      rh: map.get('rh').map(_.partial(listToAbcString, config.get('key')))
    }))
    // Let's add a rest and rough count in at the start
    .update(map => Map({
      lh: List([ 'Z2',
        Repeat(toAbc(List(Chord.notes(chordsAllLaidOut.first().first())).last() + '3', config.get('key')) + '/z/',
          timeSigProperties.getIn([config.get('timeSig'), 'duration'])).join(''),
        ...map.get('lh'),
        toAbc(List(Scale.notes(config.get('key'))).first() + '3', config.get('key')) + timeSigProperties.getIn([config.get('timeSig'), 'duration']),
        toAbc(List(Scale.notes(config.get('key'))).first() + '3', config.get('key')) + timeSigProperties.getIn([config.get('timeSig'), 'duration'])
      ]),
      rh: List(['Z2',
        'z' + timeSigProperties.getIn([config.get('timeSig'), 'duration']),
        ...map.get('rh'),
        toAbc(List(Scale.notes(config.get('key'))).filter((v, i) => i === 0 || i === 2 || i === 4).map(x => x + '5'), config.get('key')) + timeSigProperties.getIn([config.get('timeSig'), 'duration']),
        toAbc(List(Scale.notes(config.get('key'))).filter((v, i) => i === 0 || i === 2 || i === 4).map(x => x + '5'), config.get('key')) + timeSigProperties.getIn([config.get('timeSig'), 'duration'])
      ])
    }))

  console.log(Repeat(toAbc(List(Chord.notes(chordsAllLaidOut.first().first())).last() + '3', config.get('key')),
    timeSigProperties.getIn([config.get('timeSig'), 'duration'])).join(''))

  // By this point, we have a List of abc strings. Let's make them into lines.
  // That's 4 bars per line

  // Then after *that*, it needs to be made into a big ABC string with a
  // preamble. This part will likely have to be separate from the chain above

  assert.strictEqual(x.get('lh').size, x.get('rh').size,
    'The right and left hand have different bars. Help!')

  function makeLines (key, voiceName) {
    return List(x.get(key).map((val, ind, arr) => {
      if (ind % barsInALine === 0) {
        return '[V:' + voiceName + '] ' + val
      } else if (ind % barsInALine === barsInALine - 1) {
        return ' | ' + val + '| \n'
      } else {
        return ' | ' + val
      }
    }).join('').split('\n'))
  }
  const lh = makeLines('lh', 'LH')
  const rh = makeLines('rh', 'RH')

  // Then we make it into a long string with newlines
  const lines = rh
    .zip(lh)
    .reduce((acc, val, ind, arr) => acc + val[0] + '\n' + val[1] + '\n'
      , '')

  return createABCHeader({
    M: config.get('timeSig'),
    L: timeSigProperties.get(config.get('timeSig')).get('L'),
    Q: config.get('tempo'),
    K: config.get('key'),
    Z: 'Created by fiddlebacker (https://nasfarley88.gitlab.io/fiddlebacker)'
  }) + lines
}
