/* eslint-env mocha */
import * as chai from 'chai'
import { List, Map, Repeat } from 'immutable'

import chaiImmutable from 'chai-immutable'

import { toAbc, abcDurationToNumber, durationOfList } from '../js/abcconversions'

const assert = chai.assert

chai.use(chaiImmutable)

toAbc('C4', 'C major')
describe('toAbc', () => {
  context('should convert a Tonal.js note to an ABC note.', () => {
    it('should convert C5 to C', () => assert.strictEqual(toAbc('C5', 'C major'), 'C'))
    it('should convert C5 to =C', () => assert.strictEqual(toAbc('C5', 'D major'), '=C'))
    it('should convert A4 to A,', () => assert.strictEqual(toAbc('A4', 'A major'), 'A,'))
    it('should convert A3 to A,,', () => assert.strictEqual(toAbc('A3', 'A major'), 'A,,'))
    it('should convert C5, C5 to [CC]', () => assert.strictEqual(toAbc(List(['C5', 'C5']), 'C major'), '[CC]'))
  })
})

describe('abcDurationToNumber', () =>
  context('Converts ABC duration into a number', () => {
    it('should convert a blank string to 1', () =>
      assert.strictEqual(abcDurationToNumber(''), 1)
    )
    it('should convert an \'anti\'-dotted quaver to a semiquaver (assuming 4/4 time)', () =>
      assert.strictEqual(abcDurationToNumber('/<'), 0.25)
    )
    it('should convert a minim to a 2 (assuming 4/4 time)', () =>
      assert.strictEqual(abcDurationToNumber('2'), 2)
    )
  })
)

describe('durationOfList', () =>
  context('Reduces lists of notes into a single number duration', () => {
    it('should add four blank strings into number duration of 4', () =>
      assert.strictEqual(
        durationOfList(Repeat(Map({ duration: '' }), 4)),
        4
      )
    )
    it('should add 1 dotted quaver pairs into number duration of 1', () =>
      assert.strictEqual(
        durationOfList(List([
          Map({ duration: '/<' }),
          Map({ duration: '/' })
        ])),
        1
      )
    )
    it('should add 1 dotted semi-quaver pairs into number duration of 0.5', () =>
      assert.strictEqual(
        durationOfList(List([
          Map({ duration: '//<' }),
          Map({ duration: '//' })
        ])),
        0.5
      )
    )
  })
)
