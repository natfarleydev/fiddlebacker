/* eslint-env mocha */
import chai from 'chai'
import { invertDown, calcLeading, calcDarkness, getVoicing } from '../js/voicings'
import { Seq, Repeat, fromJS } from 'immutable'

import chaiImmutable from 'chai-immutable'
const assert = chai.assert

chai.use(chaiImmutable)

describe('calcLeading', () => {
  context('Test manually calculated leading against function', () => {
    it('should show identical chords have leading of 0', () => {
      assert.strictEqual(calcLeading(
        Seq(Array(3).fill('C5')),
        Seq(Array(3).fill('C5'))
      ), 0)
    })
    it('should show C5*3 and Db*3 have leading 3', () => {
      assert.strictEqual(calcLeading(
        Seq(Array(3).fill('C5')),
        Seq(Array(3).fill('Db5'))
      ), 3)
    })
    it('should show C5*3 and A4, D5, F#5 have leading 3+2+6 = 11', () => {
      assert.strictEqual(calcLeading(
        Seq(Array(3).fill('C5')),
        Seq(['A4', 'D5', 'F#5'])
      ), 3 + 2 + 6)
    })
  })
})

describe('invertDown', () => {
  it('Should invert C5, E5, G5 to C5, E5, G4.', () => {
    assert.deepStrictEqual(
      invertDown(['C5', 'E5', 'G5']),
      Seq(['G4', 'C5', 'E5'])
    )
  })
})

describe('getVoicing', () =>
  context('Getting chords with calcVoicing', () => {
    it('should get a normal C chord', () =>
      assert.deepStrictEqual(
        getVoicing(
          'C',
          Repeat('C5', 3)
        ),
        fromJS(['G4', 'C5', 'E5'])
      )
    )
    it('should produce a nice D chord', () =>
      assert.strictEqual(
        getVoicing('D'),
        fromJS(['A4', 'D5', 'F#5'])
      )
    )
    it('should produce a good A7 following a D chord', () =>
      assert.strictEqual(
        getVoicing('A7', Seq(['A4', 'D5', 'F#5'])),
        Seq(['G4', 'C#5', 'E5'])
      )
    )
  })
)

describe('calcDarkness', () =>
  context('Test darkness calculations with calcDarkness', () => {
    it('should have 0 darkness for a bright chord', () =>
      assert.strictEqual(calcDarkness(Seq(['C6', 'D6', 'E6']), 0), 0)
    )
    it('should cancel existing darkness for a bright chord', () =>
      assert.strictEqual(calcDarkness(Seq(['C6', 'D6', 'E6']), 5), 0)
    )
    it('should add 1 darkness for a dark chord', () =>
      assert.strictEqual(calcDarkness(Seq(['G4', 'B4', 'D5']), 5), 6)
    )
    it('should add 1 darkness for a dark chord', () =>
      assert.strictEqual(calcDarkness(Seq(['F#4', 'A4', 'D5']), 5), 6)
    )
  })
)
