/* eslint-env mocha */
import './tune.test'
import './voicings.test'
import './reducers.test'
import './abcconversions.test'
