/* eslint-env mocha */
// import { assert } from 'chai'
import _ from 'lodash'
import chai from 'chai'
import { Seq, Map, Repeat, List, fromJS } from 'immutable'
import { getBassNotes, chordBlockToRhythmBlocks, rhythmBlockToNotes, newGetBassNote, reduceIntoBars, chordsToAbcString } from '../js/tune'

import chaiImmutable from 'chai-immutable'
const assert = chai.assert

chai.use(chaiImmutable)

const testDataFromSite = fromJS([['D'], ['A']])

describe('getBassNotes', () =>
  context('Testing getting bass notes with getBassNotes', () => {
    it('should get an ordinary C triad', () =>
      assert.isTrue(getBassNotes('C').equals(Seq(['C3', 'E3', 'G3'])))
    )
    it('should get an ordinary C triad from a C9', () =>
      assert.isTrue(getBassNotes('C9').equals(Seq(['C3', 'E3', 'G3'])))
    )
  })
)

describe('chordBlockToRhythmBlocks', () =>
  context('Converts chord blocks into rhythm blocks', () => {
    it('should convert a single chord block into a rhythm block', () =>
      assert.deepStrictEqual(chordBlockToRhythmBlocks(
        '4/4',
        List(),
        Map({ symbol: 'D', duration: 4 })
      ),
      fromJS([
        {
          rh: [
            { rest: true, duration: '' },
            { rest: false, duration: '/<' },
            { rest: true, duration: '/' }],
          lh: [
            { rest: false, duration: '' },
            { rest: true, duration: '' }
          ],
          symbol: 'D',
          duration: 2
        },
        {
          rh: [
            { rest: true, duration: '' },
            { rest: false, duration: '/<' },
            { rest: true, duration: '/' }],
          lh: [
            { rest: false, duration: '' },
            { rest: true, duration: '' }
          ],
          symbol: 'D',
          duration: 2
        }
      ])
      )
    )
  })
)

describe('rhythmBlockToNotes', () =>
  context('Converting a rhythm block to readable notes', () => {
    it('should convert a simple rhythm block into rests', () => {
      const rhythmBlocks = fromJS([
        {
          rh: [{ rest: true, duration: '' }],
          lh: [{ rest: true, duration: '' }],
          duration: 1,
          symbol: 'D'
        }
      ])

      assert.deepStrictEqual(
        rhythmBlockToNotes(
          Map(),
          rhythmBlocks.last(),
          0,
          rhythmBlocks
        ),
        fromJS({
          rh: [{ notes: ['z'], duration: '', rest: true }],
          lh: [{ notes: ['z'], duration: '', rest: true }]
        })
      )
    })

    it('should convert a simple rhythm block into D major chord', () => {
      const rhythmBlocks = fromJS([
        {
          rh: [{ rest: false, duration: '' }],
          lh: [{ rest: false, duration: '' }],
          duration: 1,
          symbol: 'D'
        }
      ])

      assert.deepStrictEqual(
        rhythmBlockToNotes(
          Map(),
          rhythmBlocks.last(),
          0,
          rhythmBlocks
        ),
        fromJS({
          rh: fromJS([{ notes: List(['A4', 'D5', 'F#5']), duration: '', rest: false }]),
          lh: fromJS([{ notes: List(['D3']), duration: '', rest: false }])
        })
      )
    })

    it('should convert a complex rhythm block into several D major chords', () => {
      const rhythmBlocks = fromJS([
        {
          rh: [
            { rest: false, duration: '' },
            { rest: false, duration: '/<' },
            { rest: false, duration: '/' }
          ],
          lh: [
            { rest: false, duration: '' },
            { rest: false, duration: '/<' },
            { rest: false, duration: '/' }
          ],
          duration: 1,
          symbol: 'D'
        }
      ])

      assert.deepStrictEqual(
        rhythmBlockToNotes(
          Map(),
          rhythmBlocks.last(),
          0,
          rhythmBlocks
        ),
        fromJS({
          rh: [
            { notes: ['A4', 'D5', 'F#5'], duration: '', rest: false },
            { notes: ['A4', 'D5', 'F#5'], duration: '/<', rest: false },
            { notes: ['A4', 'D5', 'F#5'], duration: '/', rest: false }
          ],
          lh: [
            { notes: ['D3'], duration: '', rest: false },
            { notes: ['D3'], duration: '/<', rest: false },
            { notes: ['D3'], duration: '/', rest: false }
          ]
        })
      )
    })
  })
)

describe('newGetBassNote', () => {
  it('should produce a nice D3', () =>
    assert.strictEqual(
      newGetBassNote('D'),
      'D3'
    )
  )
  it('should produce a nice D3 after A3', () =>
    assert.strictEqual(
      newGetBassNote('D',
        fromJS([{
          rest: false,
          notes: ['A3'],
          symbol: 'D'
        }])
      ),
      'D3'
    )
  )
  it('should produce a nice A3 after D3', () =>
    assert.strictEqual(
      newGetBassNote('D', fromJS([
        {
          rest: false,
          notes: ['D3'],
          symbol: 'D'
        }
      ]),
      undefined,
      'D'),
      'A3'
    )
  )
  it('should lead into a G chord with F#3 from a D', () =>
    assert.strictEqual(
      newGetBassNote('D', fromJS([
        {
          rest: false,
          notes: ['D3'],
          symbol: 'D'
        }
      ]), 'G', 'D'),
      'F#3'
    )
  )
  it('should play the root note even though the previous note was a root', () =>
    assert.strictEqual(
      newGetBassNote('A', fromJS([
        {
          rest: false,
          notes: ['A3'],
          symbol: 'D'
        }
      ]), 'A'),
      'A3'
    )
  )
})

describe('reduceIntoBars', () =>
  context('Reduces lists of notes into bars', () => {
    it('should reduce four beats into a 4/4 bar', () =>
      assert.deepStrictEqual(
        Repeat(Map({ rest: true, duration: '' }), 4)
          .reduce(_.partial(reduceIntoBars, 4), List([])),
        fromJS([
          [
            { rest: true, duration: '' },
            { rest: true, duration: '' },
            { rest: true, duration: '' },
            { rest: true, duration: '' }
          ]
        ])
      )
    )
  })
)

describe('chordsToAbcString', () =>
  context('Converts a bunch of chords to ABC accompaniament(sp?)', () => {
    it('should produce some sort of ABC string', () =>
      assert.strictEqual(
        chordsToAbcString(Map({ A: testDataFromSite }), Map({ key: 'D major', timeSig: '4/4', sequence: 'A', tempo: 100 })),
        `M: 4/4
L: 1/4
Q: 100
K: D major
Z: Created by fiddlebacker (https://nasfarley88.gitlab.io/fiddlebacker)
%%staves (RH) (LH)
V:RH clef=treble name =""
V:LH clef=bass name =""
[V:RH] Z2 | z4 | z[A,DF]/<z/z[A,DF]/<z/ | z[A,CE]/<z/z[A,CE]/<z/| 
[V:LH] Z2 | A,,/z/A,,/z/A,,/z/A,,/z/ | [D,,]z[A,,]z | [A,,]z[E,,]z| 
[V:RH] [DFA]4 | [DFA]4
[V:LH] D,,4 | D,,4
`
      ))
  })
)
