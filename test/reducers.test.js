/* eslint-env mocha */

import * as chai from 'chai'
import { List, Map } from 'immutable'
import _ from 'lodash'
import chaiImmutable from 'chai-immutable'

import { divChordsToChordBlocksRdcr } from '../js/reducers'

const assert = chai.assert

chai.use(chaiImmutable)

const testDataFromSite = List([List(['D']), List(['A'])])

describe('divChordsToChordBlocks', () =>
  context('Tests converting chords from the HTML into usable chord blocks', () => {
    it('should convert one chord bars into duration 4 chord blocks', () =>
      assert.deepStrictEqual(
        divChordsToChordBlocksRdcr(
          4,
          List(),
          List(['D'])
        ),
        List([Map({ symbol: 'D', duration: 4 })])
      )
    )
    it('should convert two \'D\' duration 4 blocks into a single duration 8 block', () =>
      assert.deepStrictEqual(
        divChordsToChordBlocksRdcr(
          4,
          List([Map({ symbol: 'D', duration: 4 })]),
          List(['D'])
        ),
        List([Map({ symbol: 'D', duration: 8 })])
      )
    )
    it('should convert add a single \'A\' duration 4 block', () =>
      assert.deepStrictEqual(
        divChordsToChordBlocksRdcr(
          4,
          List([Map({ symbol: 'D', duration: 4 })]),
          List(['A'])
        ),
        List([
          Map({ symbol: 'D', duration: 4 }),
          Map({ symbol: 'A', duration: 4 })
        ])
      )
    )
    it('should properly reduce chors into two duration 4 blocks', () =>
      assert.deepStrictEqual(
        testDataFromSite.reduce(_.partial(divChordsToChordBlocksRdcr, 4), List([])),
        List([
          Map({ symbol: 'D', duration: 4 }),
          Map({ symbol: 'A', duration: 4 })
        ])
      ))
  })
)
